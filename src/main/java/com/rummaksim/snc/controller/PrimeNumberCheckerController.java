package com.rummaksim.snc.controller;

import com.rummaksim.snc.controller.data.IsPrimeNumberResponse;
import com.rummaksim.snc.service.PrimeNumberCheckerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PrimeNumberCheckerController {
    private final PrimeNumberCheckerService service;

    @Autowired
    public PrimeNumberCheckerController(PrimeNumberCheckerService service) {
        this.service = service;
    }

    @GetMapping("/number/{numericValue}/simple")
    public IsPrimeNumberResponse sendPrimeNumberCheckingResults
            (@PathVariable("numericValue") Integer numericValue) {
        return service.check(numericValue);
    }
}
