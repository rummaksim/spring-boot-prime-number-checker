package com.rummaksim.snc.controller;

import com.rummaksim.snc.controller.data.ApiErrorResponse;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController implements ErrorController {

    private static final String PATH = "/error";

    @GetMapping(PATH)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ApiErrorResponse error() {
        return new ApiErrorResponse(HttpStatus.NOT_FOUND.value(), "По указанному URL ресурс не найден");
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }
}