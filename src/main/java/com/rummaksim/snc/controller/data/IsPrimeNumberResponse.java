package com.rummaksim.snc.controller.data;

public class IsPrimeNumberResponse {
    private final Integer value;
    private final String message;

    public IsPrimeNumberResponse(Integer value, String message) {
        this.value = value;
        this.message = message;
    }

    public Integer getValue() {
        return value;
    }

    public String getMessage() {
        return message;
    }
}
