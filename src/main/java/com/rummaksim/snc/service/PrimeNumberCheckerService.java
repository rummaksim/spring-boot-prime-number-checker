package com.rummaksim.snc.service;

import com.rummaksim.snc.controller.data.IsPrimeNumberResponse;
import org.apache.commons.math3.primes.Primes;
import org.springframework.stereotype.Service;

@Service
public class PrimeNumberCheckerService {
    private final String PRIME_VALUE_MSG = "Число %d является простым";
    private final String NOT_PRIME_VALUE_MSG = "Число %d не является простым";

    public IsPrimeNumberResponse check(Integer value) {
        return new IsPrimeNumberResponse(value, Primes.isPrime(value) ?
                String.format(PRIME_VALUE_MSG, value) : String.format(NOT_PRIME_VALUE_MSG, value));
    }

}
